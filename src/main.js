import Vue from 'vue';
import App from './App.vue';
import {
  LayoutMain
} from './views/layouts';

import router from '@/router';
import store from './store/index';
import axios from 'axios';

import VueLodash from 'vue-lodash';
import NProgress from 'nprogress';
import Form from 'react-jsonschema-form';
import VueReact from 'vue-react';
NProgress.configure({
  showSpinner: false
});
const options = {
  name: 'lodash'
};

import Storage from 'vue-ls';
import AsyncComputed from 'vue-async-computed'

Vue.use(AsyncComputed)
const optionsVue = {
  namespace: 'vuejs__', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local' // storage name session, local, memory
};
Vue.use(VueReact);
Vue.use(VueLodash, options);
Vue.use(Storage, optionsVue);

Vue.react('JsonFrom', Form);

import 'ant-design-vue/dist/antd.css';
import './assets/css/ant-theme-custrom.css';
import 'bootstrap/dist/css/bootstrap.css';
import Antd from 'ant-design-vue';
import './assets/css/site.css';


// import './components/tools/global.less'
Vue.config.productionTip = false;
Vue.use(Antd);

const defaultRoutePath = '/dashboard';
const ACCESS_TOKEN = Vue.ls.get('ACCESS_TOKEN');
const whiteList = ['login', 'register', 'registerResult'];

router.beforeEach((to, from, next) => {
  NProgress.start();
  if (ACCESS_TOKEN) {
    if (to.path === '/auth/login') {
      next({
        path: defaultRoutePath
      });
      NProgress.done();
    } else {
      next();
      NProgress.done();
    }
  } else {
    if (whiteList.includes(to.name)) {
      next();
    } else {
      next();
      router.push({
        path: '/auth/login'
      });
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  NProgress.done(); // finish progress bar
});

new Vue({
  router,
  store,
  render: h => h(App)
  // eslint-disable-next-line prettier/prettier
}).$mount('#app');