import InstallPowerLocationDriver from './InstallPowerLocationDriver';
import InstallPowerLocationGen from './InstallPowerLocationGen';

export {
    InstallPowerLocationDriver,
    InstallPowerLocationGen
};