import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {

    ip_serve: 'http://erg.dede.go.th:3333',
    //ip_serve: 'http://103.80.51.21:3333',
    //ip_serve: 'http://data.eformthai.com',
    auth_token: '',
    //authorization:
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.auth_token = token;
    },
  },
  actions: {
    Login({
      commit,
      state
    }, dataInfo) {
      return new Promise((resolve, reject) => {
        axios
          .post(state.ip_serve + '/api/auth/login', dataInfo)
          .then((response) => {
            let token = response.data.response.token;
            let username = response.data.request.username;
            if (token) {
              Vue.ls.set('ACCESS_TOKEN', token, 1 * 24 * 60 * 60 * 1000);
              Vue.ls.set('USER_NAME', username, 1 * 24 * 60 * 60 * 1000);
              commit('SET_TOKEN', token);
            }
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    Logout({
      commit,
      state
    }) {
      return new Promise((resolve) => {
        commit('SET_TOKEN', '');
        Vue.ls.remove('ACCESS_TOKEN');
        Vue.ls.remove('USER_NAME');
        resolve();
      });
    },
  },
  getters: {
    getIpServe: (state) => state.ip_serve,
    getAuthToken: (state) => state.auth_token,
  },
});