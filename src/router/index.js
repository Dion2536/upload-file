/* eslint-disable prettier/prettier */
import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/pages/Home.vue";

import AuthLayout from "@/views/layouts/AuthLayout";
import LayoutMain from "@/views/layouts/LayoutMain";
import {
    MemberUpload,
    ListFactory,
    ListFactoryby,
    PageNotFound,
    FactoryDetails,
    Login,
    Dashboard
} from "@/views/pages";
Vue.use(Router);


export default new Router({
    mode: "history",
    // base: process.env.BASE_URL,
    scrollBehavior: () => ({
        y: 0
    }),
    routes: [{
            path: '/auth',
            name: 'auth',
            component: AuthLayout,
            redirect: '/auth/login',
            hidden: true,
            // meta: {
            //     requiresAuth: false
            // },
            children: [{
                    path: 'login',
                    component: Login
                },
                //   {
                //     path: 'register',
                //     name: 'register',
                //     component: () => import('@/views/auth/Register')
                //   }
                {
                    path: 'recover',
                    name: 'recover',
                    component: undefined
                },

            ]
        },
        {
            path: '/',
            component: LayoutMain,
            redirect: '/dashboard',
            hidden: true,
            // meta: {
            //     requiresAuth: true
            // },
            children: [{
                    path: "/dashboard",
                    name: "dashboard",
                    component: Dashboard
                },
                {
                    path: "list-factory",
                    name: "list-factory",
                    component: ListFactory
                },
                {
                    path: "list-factory-by",
                    name: "list-factory-by",
                    component: ListFactoryby
                },
                {
                    path: "list-factory/:id",
                    name: "list-factory-id",
                    component: ListFactory
                },
                {
                    path: "factory-details/:id",
                    name: "factory-details",
                    component: FactoryDetails
                },
            ]
        },
        {
            path: "/",
            name: "member-upload",
            component: MemberUpload
        },


        {
            path: "*",
            component: PageNotFound
        }

    ]
});